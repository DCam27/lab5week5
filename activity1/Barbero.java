

class Barbero {

  private String nombre;


  public Barbero(String nombre) {
    this.nombre = nombre;
  }

  public String getNombre() {
    return nombre;
  }

  public void atenderCliente(Cliente cliente) {
    cliente.setEstado(Estado.EN_ATENCION);

  }

}
