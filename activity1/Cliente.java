
class Cliente {

  private String nombre;
  private Estado estado;

  public Cliente(String nombre) {
    this.nombre = nombre;
    this.estado = Estado.EN_ESPERA;
  }

  public String getNombre() {
    return nombre;
  }

  public Estado getEstado() {
    return estado;
  }

  public void setEstado(Estado estado) {
    this.estado = estado;
  }

  @Override
  public String toString() {
    return nombre + ", " + estado;
  }
}
