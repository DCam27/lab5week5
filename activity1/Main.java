
import java.util.LinkedList;
import java.util.Queue;

public class Main {

  public static void main(String[] args) {
    Barbero julio = new Barbero("Julio");
    Barbero tulio = new Barbero("Tulio");

    Queue<Cliente> colaEspera = new LinkedList<>();
    String[] nombresClientes = {"c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8"};
    for (String nombre : nombresClientes) {
      colaEspera.add(new Cliente(nombre));
    }

    System.out.println(colaEspera);
    julio.atenderCliente(colaEspera.poll());
    tulio.atenderCliente(colaEspera.poll());
    System.out.println(colaEspera);
    julio.atenderCliente(colaEspera.poll());
    tulio.atenderCliente(colaEspera.poll());
    System.out.println(colaEspera);

  }

}
